====course_booking2 database====
====users collection====

1) db.users.find({ $or: [{firstName: "s"}, {lastname: "d" }]}).pretty();


2) db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		_id: 0
	}
)


3) db.users.find({ $and: [{department: "HR", age:{ $gte: 70} }]}).pretty();


4) db.users.find({firstName: {$regex:'s', $options: 'i'}, age:{ $lte: 30}}).pretty()


5) db.users.find();


6) Find the users with the letter 'a' (insenstive) but it only appear the details of firstName and lastName. _id will be hidden.


7) Jane Doe
   Stephen Hawking
   Neil Armstrong


8) It will find and display all the users that has admin and active both true.  

9) find the courses with the letter 'u' in name (insensitive) and with the price of greater than or equal to 13000

10)db.courses.find();